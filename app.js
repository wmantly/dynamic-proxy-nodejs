
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var api = require('./routes/api');
var hosts = require('./routes/hosts');
var myAuth = require('./myAuth');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 80);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.cookieParser('wtfdoiputhereS3CRE7'));
app.use(express.session());
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('less-middleware')({ src: path.join(__dirname, 'public') }));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', function(r, res){res.redirect('hosts')});
app.get('/users', user.list);
app.post('/users', user.addUsers);
app.get('/login', user.logInIndex);
app.post('/login', user.logIn);
app.get('/logout', user.logOut);
app.get('/hosts', hosts.list);
app.get('/api/:hostName', api.viewHost);
app.post('/api', api.addHost);
app.post('/api/:hostName', api.addHost);
app.del('/api/:hostName', api.deleteHost);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
