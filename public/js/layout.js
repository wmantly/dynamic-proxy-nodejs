$( document ).ready( function () {

	$( 'div.row' ).fadeIn( 'slow' ); //show the page

	//panel button's
	$( '.glyphicon-circle-arrow-down' ).each( function () {
		$( this ).click( function () {
			$( this ).closest( '.panel-heading' ).siblings( '.panel-body' ).slideToggle( 'fast' );
		});
	});
	$( '.glyphicon-remove-circle' ).each( function () {
		$( this ).click( function () {
			$( this ).closest( 'div.panel' ).slideUp( 'fast' );
		});
	});
	$( '.glyphicon-refresh' ).each( function () {
		$(this).click( function () {
			tableAJAX();
		});
	});
});

//ajax form submit
function formAJAX( btn, del ) {
	var $form = $(btn).closest( '[action]' ), // gets the 'form' parent
		str = $form.find( '[name]' ).serialize(); // builds query string
	/*if( !$form.validate(
		{
			form: {
				alertCount: true,
				alertCountMessage: " errors found on this form!"
			}
		}) ) {
		return false;
	}*/
	if( del ){ // delete request 
		if( confirm( 'Are you sure?' ) ) {
			str += "&deleteThis=true"; // adds delete to query string
		}else{
			event.preventDefault(); // avoid to execute the actual submit of the form
			return false; //cancels function
		}
	}
	$.post( $form.attr( 'action' ), str, function ( data ) { // sends post data to server validate 
		tableAJAX( data.action_message ); //re-populate table
	});
	eval( $form.attr( 'evalAJAX' ) ); //gets JS to run after completion
	event.preventDefault(); // avoid to execute the actual submit of the form.
	return false; // avoid to execute the actual submit of the form.
}
