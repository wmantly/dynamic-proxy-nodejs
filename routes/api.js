var extend = require('node.extend'),
    redis = require("redis"),
	client = redis.createClient();

exports.viewHost = function(req, res){
    client.HGETALL(req.params.hostName, function (err, replies) {
        res.json(extend({host: req.params.hostName}, replies));
    });
};

exports.addHost = function(req, res){
    var ip = (req.body.ip) ? req.body.ip : req.ip,
        hostName = (req.params.hostName) ? req.params.hostName : req.body.host;
    console.log(hostName);
    if(!hostName){
        res.json({action_message: "No host field"});
    }else if(req.body.deleteThis){
        client.SREM('hosts', hostName, redis.print);
        client.DEL(hostName, function (err, numRemoved) {
            res.json( {action_message:"Host <i>" + hostName + "</i> Deleted "} );
        });
    }else{

        client.SADD("hosts", hostName, function(err, replies) {
            ;
        });

        client.HSET(hostName, "ip", ip, redis.print);
        client.HSET(hostName, "updated", (new Date).getTime(), redis.print);
        res.json( {action_message:"Host <i>" + hostName + "</i> Added "} );
    }
};

exports.deleteHost = function(req, res){
    client.SREM('hosts', req.params.host, redis.print);
    client.DEL(req.params.hostName, function (err, numRemoved) {
        console.log(numRemoved);
    });
};