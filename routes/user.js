var passwordHash = require('password-hash'),
	redis = require("redis"),
	client = redis.createClient(),
	myAuth = require('../myAuth');

exports.list = function(req, res){

	myAuth.myAuth(req, res, "users");

	if(req.xhr){
		client.HKEYS("users", function (err, replies) {
	        res.send(replies);
	    });
	} else {
		res.render('users', {});
	}
};

exports.addUsers = function(req, res){

	//checks if user name is pressent
	if(req.body.username == null){ 
		res.send("No user name");
	}

	//delete user
	if(req.body.deleteThis == "true"){
		client.HDEL('users', req.body.username, redis.print);
		res.json( {action_message:"User <i>" + req.body.username + "</i> Deleted "} );
		return ;
	}

	//changed user name if needed.
	if(req.body.password){
		//sets values in redis
		var hashedPassword = passwordHash.generate(req.body.password);

		client.HSET('users', req.body.username, hashedPassword, redis.print);


		if(req.body.first){
			client.HSET( 'setting', 'first_run', '1', redis.print );
			req.session.auth = undefined;
			res.redirect('login');	;
		}

		res.json({action_message:"User <i>"+req.body.username+"</i> committed "} );
	}
	res.json({action_message:"You shound not see this..."});
};

exports.logIn = function(req, res){
	client.HGET('users', req.body.username, function(err, replies){
		var passwordHash = require('password-hash');
		if(passwordHash.verify(req.body.password, replies)){
			req.session.auth = 1;
			if(req.body.redirect){
				res.redirect(req.body.redirect);
			}
			res.json({action_message:"Your in!"} );
		}else{
			res.json({action_message:"Bad something", hash: replies} );
		}
	});
};

exports.logInIndex = function(req, res){
	res.render('login', {redirect: req.query.redirect});
};

exports.logOut = function(req, res){
	req.session.auth = undefined;
	res.redirect('login');	
};