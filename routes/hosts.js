var passwordHash = require('password-hash'),
	redis = require("redis"),
	client = redis.createClient(),
	extend = require('node.extend');

exports.list = function(req, res){
	if(!req.session.auth){
		res.redirect('login?redirect=hosts');
	}

	if(req.xhr){
		var jsonObj = {};

		client.SMEMBERS("hosts", function (err, replies) {
			var stopLength = replies.length,
				i = 0;
			replies.forEach(function(host){
				client.HGETALL(host, function(err, repliesHost){
					i++;
					jsonObj[host] = extend({host: host}, repliesHost);
					if(stopLength == i){
						res.json(jsonObj);
					}
				});

			});
		});
	} else {
		res.render('hosts', {});
	}
};